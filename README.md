# Printed circuit board business card with NFC capabilities
This is a landing page repository for people who received my nfc pcb business card (or stumbled over this link somehow, hi there!).

Here you can find the bill of materials, schematics and board designs of the pcb business card.

Once assembled, this project harvests power from a nearby NFC client (such as a phone). The harvested power goes to a 555 timer and a digital 8-bit counter. By touching the touchpad on the front side of the pcb, the counter will count up. The NFC component will also provide contact information.

![](images/businesscard_top.png)

## Bill of materials
To complete this pcb business card, you will require the following hardware components (if you were given an unpopulated board):
- NXP NTP5210 NTAG NFC device (Package: TSSOP8) x1
- ‎TI LMC555 timer (Package: SOIC8) x 1
- TI HC590A binary 8-bit counter (Package: SOIC16) x1
- Resistor 100kOhm (Package: 0603 SMD) x1
- Resistor 330Ohm (Package: 0603 SMD) x8
- Capacitor 10uF (Package: 0603 SMD) x2
- Capacitor 10nF (Package: 0603 SMD) x1

## Images

![](images/businesscard_top.png)

![](images/businesscard_bottom.png)


## References
Thanks to https://www.instructables.com/PCB-Business-Card-With-NFC/ for the idea and the nfc antenna!